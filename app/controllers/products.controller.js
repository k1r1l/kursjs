const db = require("../models")
const Products = db.products;
const Op = db.Sequelize.Op

exports.create = (req, res) => {
    if (!req.body.title || !req.body.price) {
        let field = req.body.title !== undefined ? 'Title' : 'Price'

        res.status(400).send({
            message: field + 'required'
        })
    }

    const product = {
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        expired: false,
        created_at: Date.now()
    }

    Products.create(product)
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(400).send({
                message:
                    err.message || "Some error occurred while creating the Product"
            })
        })
}

exports.update = (req, res) => {
    const id = req.params.id;

    Products.update(req.body, {
        where: {id: id}
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Product was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Product with id=" + id
            });
        });
}

exports.index = (req, res) => {
    console.log('THIS', req)
    Products.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while return all Products"
            })
        })
}

exports.show = (req, res) => {
    const id = req.params.id;

    Products.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Product with id=" + id
            });
        });
}

exports.destroy = (req, res) => {
    const id = req.params.id;

    Products.destroy({
        where: {id: id}
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Product was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Product with id=${id}. Maybe Product was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Product with id=" + id
            });
        });
}