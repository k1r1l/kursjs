module.exports = app => {
    const products = require("../controllers/products.controller.js");

    var router = require("express").Router();

    // Create a new Products
    router.post("/", products.create);

    // Retrieve all Products
    router.get("/", products.index);

    // Retrieve one Products
    router.get("/:id", products.show);

    // Update a Products with id
    router.put("/:id", products.update);

    // Delete a Products with id
    router.delete("/:id", products.destroy);

    app.use('/products', router);

    router.get('/test', (req, res)=>{
        res.send("OK")
    })
};

