const dbConfig = require('./config/db.config')

var mysql = require('mysql2')
var migration_service = require('mysql-migrations')

var connection = mysql.createPool({
    connectionLimit: 10,
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

migration_service.init(connection, __dirname + '/migrations', function (){
    console.log('finished running migrations')
})
