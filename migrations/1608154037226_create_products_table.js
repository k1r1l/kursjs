module.exports = {
    "up": "CREATE TABLE IF NOT EXISTS products (\n" +
        "    id INT AUTO_INCREMENT PRIMARY KEY,\n" +
        "    title VARCHAR(255) NOT NULL,\n" +
        "    description VARCHAR(255) NULL,\n" +
        "    price INT,\n" +
        "    expired BOOLEAN,\n" +
        ")",
    "down": "DROP TABLE products"
}